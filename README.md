# openresty_dyups
openresty + ngx_http_dyups_module


## Getting started

### Step1. modify your nginx.conf & upstream.conf
Modify your nginx.conf & upstream.conf.

### Step2. docker build
```
$ docker build -t openresty_dyups .
```

### Step3. docker run
The port is depends on your config file.
```
$ docker run -d -p 8080:8080 -p 8081:8081 -p 8088:8088 -p 8089:8089  openresty_dyups
```

### Step4. try
Add dynamic url for nginx
```
$ curl -d "server 127.0.0.1:8089;server 127.0.0.1:8088;" 127.0.0.1:8081/upstream/dyhost
```

Check
```
$ curl -H "host: dyhost" 127.0.0.1:8080
```

Get List
```
$ curl 127.0.0.1:8081/list
```

## Reference
[docker-openresty](https://github.com/openresty/docker-openresty)  
[ngx_http_dyups_module](https://github.com/yzprofile/ngx_http_dyups_module)
